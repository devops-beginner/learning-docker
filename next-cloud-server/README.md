#### Next Cloud Server

---
- Next Cloud server container.
- Create an Image from nextcloud base image. 
- Next Cloud listens to port 80, so expose the port 80
- Buld the Docker Image
```shell script
next-cloud-server % docker build -t nextcloudserver .
Sending build context to Docker daemon   2.56kB
Step 1/2 : FROM nextcloud
 ---> 7aba7d27e77d
Step 2/2 : EXPOSE 80
 ---> Running in 01042e42b266
Removing intermediate container 01042e42b266
 ---> f6a03615b0f7
Successfully built f6a03615b0f7
Successfully tagged nextcloudserver:latest
```
- Run the container with host port mapping to port 80 of server using -p flag.
```shell script
next-cloud-server % docker run --rm -p 8081:80 nextcloudserver
Initializing nextcloud 18.0.3.0 ...
Initializing finished
New nextcloud instance
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.17.0.2. Set the 'ServerName' directive globally to suppress this message
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.17.0.2. Set the 'ServerName' directive globally to suppress this message
[Sun Apr 12 17:42:52.118365 2020] [mpm_prefork:notice] [pid 1] AH00163: Apache/2.4.38 (Debian) PHP/7.3.16 configured -- resuming normal operations
[Sun Apr 12 17:42:52.118437 2020] [core:notice] [pid 1] AH00094: Command line: 'apache2 -D FOREGROUND'
172.17.0.1 - - [12/Apr/2020:17:43:11 +0000] "GET / HTTP/1.1" 200 3750 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36"
```

- Server should be up at `http://localhost:8081/`
