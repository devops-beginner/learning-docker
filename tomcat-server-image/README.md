### Tomcat Server

---
- This is a plain tomcat server with jdk installed. Idea here is explore `docker exec` and `docker attach`.
- Build the docker image using `docker build -t tomcatserver .`
- Run the container `docker run -it --rm -p 8080:80 tomcatserver tomcat`
- Open another terminal session and run `docker exec -it tomcat /bin/bash`
- We can also use `docker attach tomcat` to connect to running container.