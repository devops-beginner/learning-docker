- File named compute.js computes and displays the area of a disk using JavaScript.
- Docker image that runs this code using the node:11-alpine image that contains Node.JS. The command that runs a JavaScript program using Node.JS is node <filename>
  
- to run this
   - `docker build -t computejs .`
   - `docker run --rm computejs`
```shell script
nodejs-areacal % docker run --rm computejs
Area of a 2 cm disk:
    12.566370614359172 cm²
```
