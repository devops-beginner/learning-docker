### Python Dev Image

---
- The idea here is to create a Container with python to host a simple Flask API.
- Start with python image.
- Install Flask framework on this.
- Copy the source code
- Set the `FLASK_APP` a pointer  to flask application.
- Expose the default fault 5000.
- Run the flask application using `CMD flask run`
- Build an image from docker file using 

`docker build -t <your-dockerid>/pydevimage .`

- Run the container

`docker run --rm -it -p 5000:5000 <your-dockerid>/pydevimage`

- Once the server starts we can hit below urls.
    - http://localhost:5000
    - http://localhost:5000/api
    - http://localhost:5000/api/user/<username>
