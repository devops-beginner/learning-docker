#### Running Java App on Container

---
- To run the Java application, I first need to compile the program and then run the application.
- There are two way to approach this.
    - First one is very straight forward - Start with Linux Open JDK base image, copy and compile
     the code and then run the compiled class file. However, If I need to upload my image with
      compiled application, this approach creates an image with quite large size with all build
       artifacts and SDK, but to run the application, I really don't need the SDK to run my
        application, JRE would be good enough, so that's the second way.
     - Use multi-stage Dockerfile, to compile the application with JDK and then, copy the
      compiled artifact to the new image from Linux RRE Alpine and run the compiled application
       in new image, this is optimized approach to create an image only with the required
        components.

- Refer the Dockerfile for the first approach, and Dockerfileopt for second approach.

>Note: `WORKDIR` command/instruction in the Dockerfile help us to change the working directory
> inside the container.

>`--from` flag with the `COPY` command allow us to specify the source from previous image.

- Build the image

`docker build -t <your-dockerid>/java-app .`
- Run the container

`docker run -it --rm <your-dockerid>/java-app`
