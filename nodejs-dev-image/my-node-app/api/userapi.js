let express = require('express');
let router = express.Router();

/* GET users listing. */
router.get('/', function(req, res) {
    res.status(200).json({message:"Welcome!"});
});

router.get('/user', function (req, res) {
    let userName = req.query.name;
    res.status(200).json({message: "Welcome "+userName});
});

router.get('/user/:name', function (req, res) {
    let userName = req.params.name;
    res.status(200).json({message: "Welcome "+userName});
});

module.exports = router;
