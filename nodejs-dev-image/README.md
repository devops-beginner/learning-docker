#### Node.js Dev Image

---
- I start with the nodejs slim image.
- Creating a directory for application and change the working directory to this.
- Copy the application to working directory.
- Install dependencies and expose the port.
- Run the application.

```shell script
# Build the image
docker build -t vhegde49er/my-node-app .
Sending build context to Docker daemon  35.84kB
Step 1/7 : FROM node:12-slim
12-slim: Pulling from library/node
48839397421a: Pull complete 
cbb6511d79bf: Pull complete 
c12189b0632b: Pull complete 
877c28185c7e: Pull complete 
04e1e3945ae6: Pull complete 
Digest: sha256:6e0c4e231adad3a2e14d5bcb738a8e86b7d9b66935734f52b4258e7750ccfb01
Status: Downloaded newer image for node:12-slim
 ---> d8c2995a7f0b
Step 2/7 : RUN mkdir -p /usr/src/myapp
 ---> Running in 4e356cba561c
Removing intermediate container 4e356cba561c
 ---> 96caf77314da
Step 3/7 : WORKDIR /usr/src/myapp
 ---> Running in 571633d95fd5
Removing intermediate container 571633d95fd5
 ---> 12bb9eca7007
Step 4/7 : COPY my-node-app/ /usr/src/myapp
 ---> d2055749fdcc
Step 5/7 : RUN npm install
 ---> Running in d354b29ea94d
added 55 packages from 41 contributors and audited 140 packages in 41.863s
found 0 vulnerabilities

Removing intermediate container d354b29ea94d
 ---> f0daadba101e
Step 6/7 : EXPOSE 3000
 ---> Running in 6ede240811f1
Removing intermediate container 6ede240811f1
 ---> 92720f8108d4
Step 7/7 : CMD npm start
 ---> Running in c0db6c26fb78
Removing intermediate container c0db6c26fb78
 ---> df962b8c9aac
Successfully built df962b8c9aac
Successfully tagged vhegde49er/my-node-app:latest

# Running the container and application
nodejs-dev-image % docker run -it --rm -p 3000:3000 vhegde49er/my-node-app

> my-node-app@0.0.0 start /usr/src/myapp
> node ./bin/www

Listening on port 3000
GET / 304 3.755 ms - -
```
- Once the application is running, we can hit the below URLs
    - http://localhost:3000
    - http://localhost:3000/api/user/Username
    - http://localhost:3000/api/user?name=Username