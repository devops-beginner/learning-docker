## NGNIX Container hosting a Web page

- I need a nginx image, so Creating the image from nginx base image.
`
FROM nginx:1.15
`
- I need to copy the index.html to server directory in nginx, so I will use the COPY command.
`COPY index.html /usr/share/nginx/html`

- The build context is basically the directory you provide to the docker build command
- Use below commands to build and run the container.
    - `docker build -t nginxserver`
    - `docker run --rm -it -p 8081:80 nginxserver`

- The above commands build a webserver from the Dockerfile file instructions then start a container that listens to my machine’s 8081 port and redirect the incoming connections to the container’s 80 port. http://localhost:8081/ should serve you an index page.

> The -it switch allows to stop the container using Ctrl-C from the command-line.

> The –rm switch ensures that the container is deleted once it has stopped