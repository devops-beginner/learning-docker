#### Image accepting Parameters

---
- expects a diameter environment variable to be available and displays the area of a circle for that given diameter.

- A Docker image that runs compute.js code using the node:11-alpine image that contains Node.JS. 
- Running two containers with different values for the diameter would be a good demonstration. 

- Build the image using the command ` docker build -t paramcontainer .`
- Running the Container
```shell script
param-container % docker run --rm paramcontainer
Area of a circle with radius 5 is 78.53981633974483

# Area is computed with default diameter 10 as environment variable diameter was not passed.

param-container % docker run --rm -e diameter=45 paramcontainer
Area of a circle with radius 22.5 is 1590.4312808798327
```
