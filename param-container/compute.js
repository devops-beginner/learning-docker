let radius = process.env.diameter / 2;
let area = Math.pow(radius, 2) * Math.PI;
console.log(`Area of a circle with radius ${radius} is ${area}`);