- All Dockerfiles need to start from a base Linux image.
- Dockerfile has a hub (http://hub.docker.com) where you can see all Images submitted by users and where you can submit your own images. This is like a github for Dockerfile images.
-I will start from java:8-jre. This is an ubuntu machine with java 8 installed on it.
`FROM java:8-jre`

- The keyword here is **FROM**.

- The KEYWORD ENV it let us specify some Linux environment variables
- Here we will set CATALINA_HOME (the home of the tomcat server)
```shell script
ENV CATALINA_HOME /usr/local/tomcat
ENV PATH $CATALINA_HOME/bin:$PATH
```

- RUN lets you run Linux commands inside the image.
- The \ symbol allows you to continue the command onto the next line
- The && allows you to string multiple commands together. This is preferred over having a RUN statement for each command.
- Here I will navigate into /usr/local then download tomcat from the server then decompress it and rename it to /tomcat.
- The final path to tomcat will be /usr/local/tomcat.
```shell script
RUN cd /usr/local \
    && wget https://apache.claz.org/tomcat/tomcat-9/v9.0.34/bin/apache-tomcat-9.0.34.tar.gz \
    && tar xzvf apache-tomcat-9.0.34.tar.gz \
    && mv apache-tomcat-9.0.34/ tomcat/
```

- The EXPOSE command I tell my future container to expose 8080 port to the outside
- 8080 is the default port for tomcat.
`EXPOSE 8080`

- CMD sets the first command that will run when you create containers from the resulting image.
- Here, I will start tomcat once the container is called
`CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]`