## Learning-Docker

- [Basics](#basics)
- [Listening for Incoming Connections](#listening-for-incoming-connections)
- [Using Volumes](#using-volumes)
- [Creating an Image](#creating-image)
    - [Creating an Image with files](#creating-an-image-with-files)
    - [Tags](#tags)
    - [Environment Variables](#environment-variables)
    - [Storage](#storage)
    - [Networking](#networking)
    - [Java Application on Docker image](#running-java-application-on-docker-image)
    - [Python Application on Docker image](#running-python-application-on-docker-image)
    - [Node.js Application on Docker image](#running-nodejs-application-on-docker-image)
 - [Publishing Docker Images](#publishing-docker-images)
 - [Restart Mode](#restart-mode)
 - [Connecting to a running Container](#connect-to-a-running-container)
 
## Basics
- All Dockerfiles need to start from a base Linux image.
- `FROM` keyword is used to create the image with a base / pull an image.
- `ENV` Keyword let us specify Linux environment variables.
- `RUN` Keyword let us run Linux commands
- `EXPOSE` Keyword indicate future container to expose this port outside.
- `CMD` Sets the first command that will run when a container is created from this image.
- To build the docker image, from the Docker file directory run `docker build -t/tag <tag_name> .` here is the PATH to build the image file. The image build using this command will be stored locally.

> *Note the dot at the end of the command above. It specifies which path is used as the build context (more about that later), and where the Dockerfile is expected to be found. Should my Dockerfile have another name or live elsewhere, I can add a -f switch in order to provide the file path.*

- `docker images` will give the list of images
```shell script
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
tomcat              latest              24b60eaf8397        7 seconds ago       334 MB
java                8-jre               e44d62cf8862        8 months ago        311 MB
```
- `docker ps` shows the running containers, option -a will show all the containers.
```shell script
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```
`docker logs` retrieves the logs of a container, even when it has stopped
`docker inspect`: gets detailed information about a running or stopped container
`docker stop`: deletes a container that is still running
`docker rm`: deletes a container

- `docker --help` will show the detailed usage of commands and options. 
```shell script
Usage:	docker [OPTIONS] COMMAND

A self-sufficient runtime for containers

Options:
      --config string      Location of client config files (default "/Users/vshantha/.docker")
  -c, --context string     Name of the context to use to connect to the daemon (overrides DOCKER_HOST env var and default context set with "docker context use")
  -D, --debug              Enable debug mode
  -H, --host list          Daemon socket(s) to connect to
  -l, --log-level string   Set the logging level ("debug"|"info"|"warn"|"error"|"fatal") (default "info")
      --tls                Use TLS; implied by --tlsverify
      --tlscacert string   Trust certs signed only by this CA (default "/Users/vshantha/.docker/ca.pem")
      --tlscert string     Path to TLS certificate file (default "/Users/vshantha/.docker/cert.pem")
      --tlskey string      Path to TLS key file (default "/Users/vshantha/.docker/key.pem")
      --tlsverify          Use TLS and verify the remote
  -v, --version            Print version information and quit

Management Commands:
  builder     Manage builds
  config      Manage Docker configs
  container   Manage containers
  context     Manage contexts
  image       Manage images
  network     Manage networks
  node        Manage Swarm nodes
  plugin      Manage plugins
  secret      Manage Docker secrets
  service     Manage services
  stack       Manage Docker stacks
  swarm       Manage Swarm
  system      Manage Docker
  trust       Manage trust on Docker images
  volume      Manage volumes

Commands:
  attach      Attach local standard input, output, and error streams to a running container
  build       Build an image from a Dockerfile
  commit      Create a new image from a container's changes
  cp          Copy files/folders between a container and the local filesystem
  create      Create a new container
  diff        Inspect changes to files or directories on a container's filesystem
  events      Get real time events from the server
  exec        Run a command in a running container
  export      Export a container's filesystem as a tar archive
  history     Show the history of an image
  images      List images
  import      Import the contents from a tarball to create a filesystem image
  info        Display system-wide information
  inspect     Return low-level information on Docker objects
  kill        Kill one or more running containers
  load        Load an image from a tar archive or STDIN
  login       Log in to a Docker registry
  logout      Log out from a Docker registry
  logs        Fetch the logs of a container
  pause       Pause all processes within one or more containers
  port        List port mappings or a specific mapping for the container
  ps          List containers
  pull        Pull an image or a repository from a registry
  push        Push an image or a repository to a registry
  rename      Rename a container
  restart     Restart one or more containers
  rm          Remove one or more containers
  rmi         Remove one or more images
  run         Run a command in a new container
  save        Save one or more images to a tar archive (streamed to STDOUT by default)
  search      Search the Docker Hub for images
  start       Start one or more stopped containers
  stats       Display a live stream of container(s) resource usage statistics
  stop        Stop one or more running containers
  tag         Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE
  top         Display the running processes of a container
  unpause     Unpause all processes within one or more containers
  update      Update configuration of one or more containers
  version     Show the Docker version information
  wait        Block until one or more containers stop, then print their exit codes
```

- `docker COMMAND --help` will give more information on specific command. Also, [Docker Reference page](https://docs.docker.com/engine/reference/commandline/cli/) is another great resource. 

- The alpine image is a very small Linux image that does enough for our purpose. (new-computer-that-you-trash)

Some of the commands illustration.
```shell script
# We are basically asking for a container to be created using the alpine image, 
# and for the container to execute the printenv command that is one of the 
# binary programs packed in the alpine image.
docker run alpine printenv

(base) vshantha@vshantha-Mac ~ % docker run alpine printenv 
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=f3ad7fdcec8b
HOME=/root

# Command to remove all the stopped containers.
docker container prune -f

```

- To disconnect while allowing the long-lived container to continue running in the background, we use the -d or –detach switch on the docker run command.
```shell script
 ~ % docker run -d alpine ping www.google.com
63675b21258c6cd7599064fc88fdc871106b387a30c7131ba157bb6cf23f53cb

# In the above command, we are running alpine container and running ping command inside the container.
# However, this ping is never ending hence, we can run this 
# in detach mode using -d / -detach flag

(base) vshantha@vshantha-Mac ~ % docker ps
CONTAINER ID        IMAGE               COMMAND                 CREATED             STATUS              PORTS               NAMES
63675b21258c        alpine              "ping www.google.com"   3 minutes ago       Up 3 minutes                            youthful_lichterman
```
- In the above example, we can see that container id on the terminal console is 63675b21258c6cd7599064fc88fdc871106b387a30c7131ba157bb6cf23f53cb. However,  ps command container id has only initial part of it i.e 63675b21258c.
- We can also use first 4-5 characters of the container id. Consider the below example for stopping and removing the image.
```shell script
vshantha@vshantha-Mac ~ % docker stop 6367
6367
(base) vshantha@vshantha-Mac ~ % docker rm 6367  
6367
(base) vshantha@vshantha-Mac ~ % docker ps -a
CONTAINER ID        IMAGE               COMMAND                 CREATED             STATUS                      PORTS               NAMES
443bdf25b7a4        alpine              "ping www.google.com"   10 minutes ago      Exited (1) 10 minutes ago                       tender_keller
6d67523f6b6f        alpine              "ping google.com"       12 minutes ago      Exited (1) 10 minutes ago                       unruffled_hamilton
c0d5c6bda5a4        alpine              "ping www.docker.com"   13 minutes ago      Exited (1) 12 minutes ago                       jovial_mendel
f3ad7fdcec8b        alpine              "printenv"              16 minutes ago      Exited (0) 16 minutes ago                       wizardly_keldysh
edc17edc83fd        alpine              "printenv"              17 minutes ago      Exited (0) 17 minutes ago                       upbeat_rhodes
(base) vshantha@vshantha-Mac ~ % docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
(base) vshantha@vshantha-Mac ~ % 

# notice that there is no running container. 
```
- `-it` flag can be used with run commad to allow us to stop the container using **_"Ctrl + C"_**
- `docker stats` command will provide live list of running containers plus information about how many resources they consume on the host machine. Like a docker ps extended with live resource usage data.
- Unused images: images that are not referenced by other images or containers.
- Dangling images: images that have no name. This happens when you docker build an image with the same tag as before, the new one replaces it and the old one becomes dangling.
- There are some ways to reclaim the space consumed by Docker Images. Most of these commands are interactive in nature to get the confirmation. However, we can use `-f` flag to force the command.

```shell script
docker container prune -f
docker volume prune -f
docker image prune -f

# if you want to remove all unused images 
docker image prune --all
```

## Listening for Incoming Connections
- Suppose I want to run the NGINX web server. It listens for incoming HTTP requests on port 80 by default. If I simply run the server, my machine does not route incoming requests to it unless I use the -p switch on the docker run command.
- The -p switch takes two parameters
    - The incoming port you want to open on the host machine.
    - The port to which it should be mapped inside the container.

```shell script
(base) vshantha@vshantha-Mac ~ % docker run -d -p 8085:80 nginx
75844f0d3d15136d1b674a2f45ec8dc5a779500a54162b42369ff65a41e1ce47
(base) vshantha@vshantha-Mac ~ % docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
75844f0d3d15        nginx               "nginx -g 'daemon of…"   12 seconds ago      Up 12 seconds       0.0.0.0:8085->80/tcp   reverent_kalam


# For instance, here is how I state that I want my machine to listen for incoming connections on port 
# 8085 and route them to port 80 inside a container that runs NGINX:

docker run -p 8080:8080 jenkins

# The above command pulls jenkins image and runs container.
# Jenkins defaults to 8080 port, so we are mapping host system's 8080 to Jenkin's 80

# In th above examples I could have add the -d / -detach flag.
# In that case, the server's would have run in the background.
```

- Running a Nextcloud server in a container. The image I need to use is nextcloud and it listens by default on port 80.
```shell script
(base) vshantha@vshantha-Mac ~ % docker run -d -p 8081:80 nextcloud
Unable to find image 'nextcloud:latest' locally
latest: Pulling from library/nextcloud
c499e6d256d6: Already exists 
3a635b94b3b9: Pull complete 
cf28be682a33: Pull complete 
b7118ab6e551: Pull complete 
925f628a16b8: Pull complete 
a77cff9973b5: Pull complete 
9b4f44173a15: Pull complete 
c9bd6c436ae3: Pull complete 
399316af3112: Pull complete 
3cea11f54c61: Pull complete 
1ebb66998d8f: Pull complete 
ac288a1a6e08: Pull complete 
776d750cfd59: Pull complete 
a997f5c2f033: Pull complete 
714f886d18cb: Pull complete 
539fb50a16a1: Pull complete 
5e19175d1725: Pull complete 
d972c6c43ec4: Pull complete 
063f52635f71: Pull complete 
3d9d963a00b6: Pull complete 
5d7183414ca7: Pull complete 
Digest: sha256:66186a90c2a38d491b1098ad1a63448901f47db2c8572a0ed815c8717a6f573b
Status: Downloaded newer image for nextcloud:latest
b70e4d6c68255fe67c3753b5c28d6cf7e290aa6fc944a9d433986782b091e235

# up on successful creating of container and nextcloud server would be 
# started inside the container and listen to port 80. i.e http://localhost:8081

# stop and remove container

docker stop <id>
docker rm <id>
```
## Using Volumes
- When a container writes files, it writes them inside of the container. Which means that when the container dies (the host machine restarts, the container is moved from one node to another in a cluster, it simply fails, etc.) all of that data is lost.
- Using a volume, you map a directory inside the container to a persistent storage. Persistent storages are managed through drivers, and they depend on the actual Docker host. The volumes can be Azure File Storage on Azure or Amazon S3 on AWS as well.
- With Docker Desktop, you can map volumes to actual directories on the host system, this is done using the -v switch on the docker run command.
- Suppose you run a MySQL database with no volume, any data stored in that database will be lost when the container is stopped or restarted. In order to avoid data loss, you can use a volume mount:
```shell script
# mysql without volume
docker run -d mysql:5.7

# mysql with volume mounted
docker run -v /my/directory:/var/lib/mysql -d mysql:5.7

# It will ensure that any data written to the /var/lib/mysql directory inside the 
# container is actually written to the /your/dir directory on the host system. This 
# ensures that the data is not lost when the container is restarted.
```
`/my/directory` - directory on host machine. 

`/var/lib/mysql` - volume to be mounted on.

- When an image is pushing to a registry, its name must be:
`<repository_name>/<name>:<tag>`
    - tag is optional; when missing, it is considered to be latest by default
    
    - repository_name can be a registry DNS or the name of a registry in the Docker Hub
    
- Although the docker run command downloads images automatically when missing, you may want to trigger the download manually. To do this, you can use the `docker pull` command. A pull command forces an image to download, whether it is already present or not.
## Creating Image
- Containers are created from images. Inside our images, we can stuff our programs and their dependencies so that multiple containers can be created from those images and live happily ever after.
- A Docker image is created using the `docker build` command and a ***Dockerfile*** file. The
 Dockerfile file contains instructions on how the image should be built.
 
 (img_placeholder)
> The Dockerfile file can have any name. Naming it Dockerfile makes it easier for others to understand its purpose when they see that file in your project. It also means we don’t need to state the file name when using the docker build command.

-[Creating hello-world image](/hello-world/Dockerfile)

Create an image overview:

- Create an Image
    - Create a file named Dockerfile
    - Run a docker build command
- Run a container from the image created

-[Java and Tomcat image](/java-image/)

#### Creating an Image with files

- Creating an image that includes a web server that serves the index.html page over HTTP. NGINX is a good candidate. Using the debian base image and add instructions to my Dockerfile file that install NGINX is the way, but it’s easier strat with base images that are already configured and tested. The Docker Hub contains an NGINX image where NGINX has already been installed with a configuration that serves files found in the /usr/share/nginx/html directory.

-[Image with Index.html](/ngnix-index/)

##### Tags
- Every image follows the naming convention `<repository_name>/name:<tag>`. 
    - tag is optional; when missing, it is considered to be latest by default
    - repository_name can be a registry DNS or the name of a registry in the Docker Hub.
##### Environment Variables    
- In order to provide an environment variable’s value at runtime, you simply use the `-e name=value` parameter on the docker run command.
- It’s good practice to add an ENV instruction for every environment variable your image expects since it documents your image.
- Sample usage / example of using environment variable [here](/env-demo/)
- Another usage of enabling an Image to be parameterized [here](/param-container/)

##### Storage
- Its  good idea to create images that result in stateless containers that rely on external data stores, one such example might be file system.
- We can use `VOLUEME` instruction as `VOLUME /path/to/directory` in out Dockerfile, here  **_/path/to/directory_** is a path to a directory used **_inside the image_**.
- When a container is created using the docker run command, the -v flag can be used to map this directory (`/path/to/directory`) to an actual volume on the host system.

##### Networking
- `EXPOSE` command can be use for documenting  which ports should be redirected to the outside world.
- For an example, when I host the the server software such as Tomcat, and NGINX  in my Container, these servers usually listen to the port 80. So, we can expose the port 80  using `EXPOSE`.

`EXPOSE 80` 

- Just exposing port will NOT open a port to the outside world when a container is created from that image. While creating a container, one will need to **explicitly bind that port to an actual port of the host machine using the -p flag of the docker run command**

- Some of the examples are
    - [Next Cloud Server](/next-cloud-server)
    - [NGNIX Container hosting a Web page](/ngnix-index)
    - [Running Tomact Server](/java-image)
- Reference and More information [here](https://docs.docker.com/engine/reference/builder/)
## Publishing Docker Images

- There are two ways to have an image available on your machine:
    - Create an image using the docker build command.
    - Another way is get an image from a Registry, using the `docker pull` command or implicitly when using the `docker run` command for an image that is not available locally.

- As I want my images to run on other machines, I've to make sure that, my image is availabe for all.So,my option is to publish my images to a Registry. When the other machines need to create containers from my images, they will simply pull them from the Registry.

- A Docker Registry is an image store (like a GIT) that offers the following functions:
    - Ability to store various images.
    - Ability to store various tags for the same image.
    - An HTTP API that allows pushing images from a machine that produces them, or pull images to a machine that runs containers from those images.
    - TLS-secured connection to the API to avoid security attacks.
    
- There are many registries available, we can use public registry such as [Docker Hub](https://hub.docker.com/), or any private registries. 

- In general, following are step by step process to publish an image.
    1. Build an image (using `docker build`) with the appropriate prefix name or tag (docker tag).
    2. Log into the Registry (using `docker login`)
    3. Push the image into the Registry (using `docker push`)

- Naming convention for an image to be published is - `<short-name>/<name>:tag`. Here, short-name is your docker id. 
- There are 2 ways to set the proper name for your image.
    - If you've not created your image yet, you can run the build command to create an image with appropriate name. (Using build command to map an name of existing image result in duplicating the same image which might not be a good idea).
    - If you've created an image already, you can update an appropriate name using `docker tag` command. Using tag command will not duplicate the image but it creates a new name for the same image. Now, if we do `docker ps` we can see same image with same id twice under 2 different names. 
 
Example - `docker tag nextcloudserver <your-docker-id>/nextcloudserver`
 
 - Logging into docker account - `docker login` this command will accept credentials interactively.
- Push the image `docker push <your-docker-id>/nextcloudserver`
 
- The docker push command is smart enough to push only the bits which are differ from original nextcloud image.
- Private registries enable you to have more control over who can access your images.
- Size of an image affected by several factors, some of them are
    - Files included in the Image
    - The base image size.
    - Image layers.
- A **_.dockerignore_** can be added at the root of build context lists files and folders that should be excluded from the build.
- The base image in FROM instruction at the top of Dockerfile file is part of the image you build.
- **_Image Layers:_** When creating an image, Docker reads each instruction in order and the resulting partial image is kept separate; it is cached and labeled with a unique ID. Such caching is very effective because it is used at different moments of an image life.
  
    - In a future build, Docker will use the cached part instead of recreating it as long as it is possible.
  
    - When pushing a new version of the image to a Registry, the common part is not pushed.
  
    - When pulling an image from a registry, the common part you already have is not pulled.
  
The caching mechanism can be summed up as follows: when building a new image, Docker will try its best to skip all instructions up to the first instruction that actually changes the resulting image. All prior instructions result in the cached layers being used.

- Using the images makes our life much easier, for the purpose of development, we can just use and trash any system as easily as possible. 
- We can easily build the container, pish to ci/cd and then depoly to target machine.

Buld and Deliver on CI/CD host.
```shell script
docker build -t vhegde49er/nginxserver .
docker push vhegde49er/nginxserver
```
Deploy on the target machine.
```shell script
docker run --rm -it -p 8080:80 vhegde49er/nginxserver
```
- When we build an image, we are actually running software inside the container, the RUN command runs inside the container.
- **_Multi-Stage Dockerfile:_** While dealing with large images which consists of source code, arifacts and required metadata or data, we can use the Dockerfile with distinct sections. Below is the syntax for multi stage Dockerfile.
```shell script
FROM fat-image AS builder
...

FROM small-image
COPY --from=builder /result .
...
```
-  The above instructions define 2 images, but only the last one will be kept as the result of `docker build` command. 
- The filesystem that has been created in the first image, named builder, is made available to the second image.
- The --from argument of the COPY command, states that the /result folder from the builder image will be copied to the current working directory of the second image.
- This technique allows us to benefit from the tools available in fat-image while getting an image with only the environment defined in the small-image it’s based on

##### Running Java Application on Docker image
- Refer the example [here](/java-dev-image/)
##### Running Python Application on Docker image
- Refer the example [here](/python-dev-image/)
##### Running Node.js Application on Docker image
- Refer the example [here](/nodejs-dev-image/)

#### Restart Mode
-  Restart mode ensures that containers are always running until explicitly told not to.
- We can use `--restart` flag while creating the container using docker run. Consider the below example
```shell script
docker run -p 8080:80 --restart always myserver
```
- The above server always runs as we use restart mode with `always` flag, it will not stop. The docker stop command will not work here as well. 
- To enable only docker stop, we can use restart with unless-stopped flag, as shown below.
```shell script
docker run -p 8080:80 --restart unless-stopped myserver
``` 

#### Connect to a running container

- Couple of ways to connect to a running container.
    - Using `docker exec` command, we can connect to the machine's bash whcich is running inside the container. This example [here](/tomcat-server-image/) show's the demonstration of connecting to the bash shell of container running tomcat server.
    - Using `docker attach <container-name-or-id>`, once the command is executed, we will be working in the container.
