Using ENV in Docker
---
- Create an image that can ping any given site.
- The script ping.sh uses environment variable `host` which will be defined in the Dockerfile.

- In the Dockerfile, creating an Image from Debian linux 8. 
- Defining the Environment variable host
- Copying the ping.sh to root path of the container.
- Finally running the script.

> Important point here to notice is, the ping.sh uses environment variable host, which is specified in the Dockerfile using `ENV name=value`. Or we can also pass the value for this `ENV host` runtime while running the container as shown in the below.

- Command to build the script

    `docker build -t pinger .`

- Command to run the Container

    `docker run -rm pinger`
- To pass the host name during run time.

    `docker run --rm -e host=bing.com pinger`
- Notice, in the above command, I used `-e` flag with run command to pass the environment variable.


```shell script
(base) vshantha@vshantha-Mac env-demo % docker build -t pinger .               
Sending build context to Docker daemon  4.608kB
Step 1/4 : FROM debian:8
 ---> c1045f0373cd
Step 2/4 : ENV host=google.com
 ---> Using cache
 ---> 667e418cf7e1
Step 3/4 : COPY ping.sh .
 ---> Using cache
 ---> 4e53e917c9ea
Step 4/4 : CMD sh ping.sh
 ---> Using cache
 ---> 03a5f739278f
Successfully built 03a5f739278f
Successfully tagged pinger:latest
(base) vshantha@vshantha-Mac env-demo % docker run --rm pinger                 
Pinging google.com . . .
PING google.com (74.125.21.101) 56(84) bytes of data.
64 bytes from yv-in-f101.1e100.net (74.125.21.101): icmp_seq=1 ttl=37 time=8.29 ms
64 bytes from yv-in-f101.1e100.net (74.125.21.101): icmp_seq=2 ttl=37 time=15.7 ms
64 bytes from yv-in-f101.1e100.net (74.125.21.101): icmp_seq=3 ttl=37 time=15.7 ms
64 bytes from yv-in-f101.1e100.net (74.125.21.101): icmp_seq=4 ttl=37 time=15.3 ms
64 bytes from yv-in-f101.1e100.net (74.125.21.101): icmp_seq=5 ttl=37 time=15.3 ms
64 bytes from yv-in-f101.1e100.net (74.125.21.101): icmp_seq=6 ttl=37 time=14.9 ms
64 bytes from yv-in-f101.1e100.net (74.125.21.101): icmp_seq=7 ttl=37 time=15.0 ms
64 bytes from yv-in-f101.1e100.net (74.125.21.101): icmp_seq=8 ttl=37 time=14.6 ms
64 bytes from yv-in-f101.1e100.net (74.125.21.101): icmp_seq=9 ttl=37 time=15.6 ms
64 bytes from yv-in-f101.1e100.net (74.125.21.101): icmp_seq=10 ttl=37 time=15.3 ms

--- google.com ping statistics ---
10 packets transmitted, 10 received, 0% packet loss, time 9032ms
rtt min/avg/max/mdev = 8.290/14.608/15.727/2.136 ms
(base) vshantha@vshantha-Mac env-demo % docker run --rm -e host=bing.com pinger
Pinging bing.com . . .
PING bing.com (13.107.21.200) 56(84) bytes of data.
64 bytes from 13.107.21.200: icmp_seq=9 ttl=37 time=15.6 ms
64 bytes from 13.107.21.200: icmp_seq=10 ttl=37 time=15.9 ms

--- bing.com ping statistics ---
10 packets transmitted, 2 received, 80% packet loss, time 9222ms
rtt min/avg/max/mdev = 15.696/15.822/15.949/0.178 ms

```